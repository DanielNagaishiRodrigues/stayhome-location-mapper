#!/usr/bin/pythnon3
# coding: utf-8

import uuid
from datetime import datetime
from functools import singledispatch


class LocationMapper:
    table_name = 'StayHome_LocationMapper'
    id_attribute = 'location_id'

    def __init__(self, location_id, customer_id, campaign_id, latitude, longitude, created_at, status, processed):
        self.location_id = location_id
        self.customer_id = customer_id
        self.campaign_id = campaign_id
        self.latitude = latitude
        self.longitude = longitude
        self.created_at = created_at
        self.status = status
        self.processed = processed

    def generate_id(self):
        self.location_id = str(uuid.uuid4())

    def to_dict(self):
        return vars(self)


@singledispatch
def sanitize(data):
    return data


@sanitize.register(datetime)
def _(data):
    return data.isoformat()
