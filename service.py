#!/usr/bin/python3

import repository
from exceptions import RequestInvalid
from models import LocationMapper


def save(location):
    # if location['campaign_id'] == '':
    #     raise RequestInvalid('Campaign ID Inválido')
    #
    # if location['customer_id'] == '':
    #     raise RequestInvalid('Customer ID Inválido')
    #
    # if location['longitude'] == '0.0':
    #     raise RequestInvalid('Longitude Inválida')
    #
    # if location['latitude'] == '0.0':
    #     raise RequestInvalid('latitude Inválida')

    location = LocationMapper(location_id=None, processed='0', **location)
    location.generate_id()
    return repository.save(LocationMapper.table_name, location)
