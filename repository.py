#!/usr/bin/python3
# coding: utf-8

from dynamo_engine import table


def save(table_name, item):
    try:
        table(table_name).put_item(Item=item.to_dict())
        return item.location_id
    except Exception as e:
        print(f"[!!] save - Erro - {e}")
        raise e
