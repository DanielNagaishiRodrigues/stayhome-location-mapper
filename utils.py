import json
import os


def is_authenticated(event_headers):
    return not (event_headers is None or
                'x-api-token' not in event_headers.keys() or
                event_headers['x-api-token'] != os.getenv('TOKEN_API'))


def jsonify_body(body):
    return json.dumps(body)


def response_nok(status_code, message):
    return response(status_code, {"message": message})


def response(status_code, body):
    return {"statusCode": status_code, "body": jsonify_body(body), "isBase64Encoded": False}
