#!/usr/bin/python3
# coding: utf-8

import boto3


def table(table_name):
    dynamodb_table = boto3.resource('dynamodb').Table(table_name)
    return dynamodb_table
