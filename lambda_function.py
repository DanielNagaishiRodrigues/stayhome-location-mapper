import json

from botocore.exceptions import ClientError

from exceptions import LocationMapperException
from service import save
from utils import response, response_nok, is_authenticated


def lambda_handler(event, context):
    print(json.dumps(event))

    if not is_authenticated(event["headers"]):
        return response_nok(401, "Não autorizado")

    try:
        body = json.loads(event['body'])
        return response(201, save(body))
    except LocationMapperException as lme:
        print(f"[!!] lambda_handler - LocationMapperException - {lme.message}")
        return response_nok(lme.http_code, lme.message)
    except ClientError as ce:
        print(f"[!!] lambda_handler - ClientError - {ce}")
        return response_nok(500, ce.response['Error']['Message'])
    except (TypeError, KeyError) as e:
        print(e)
        return response_nok(400, "Bad Request")
    except Exception as e:
        print(e)
        return response_nok(500, "Internal Server Error")

# with open('scripts/json_test.json') as event_json:
#     data = json.load(event_json)
#
# lambda_handler(event=data, context=None)
