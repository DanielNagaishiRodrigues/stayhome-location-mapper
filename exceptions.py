from utils import response


class LocationMapperException(Exception):

    def __init__(self, http_code, message):
        self.http_code = http_code
        self.message = message
        super(LocationMapperException, self).__init__(message)

    def to_http_response(self):
        print("[ERROR] {} -> {}".format(self.http_code, self.message))
        return response(self.http_code, {'message': self.message})


class LocationMapperNotValid(LocationMapperException):
    def __init__(self):
        super(LocationMapperNotValid, self).__init__(400, "Localização inválida!")


class LocationMapperNotFound(LocationMapperException):
    def __init__(self):
        super(LocationMapperNotFound, self).__init__(400, "Localização não encontrada!")


class RequestInvalid(LocationMapperException):
    def __init__(self, message):
        super(RequestInvalid, self).__init__(400, message)
